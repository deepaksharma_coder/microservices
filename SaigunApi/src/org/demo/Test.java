package org.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Test{
	
	public static void main(String[] args) throws CloneNotSupportedException, IOException {
		// TODO Auto-generated method stub
		File dir=new File("D:\\DOCS");
		dir.mkdir();
		File fileName=new File(dir,"demo.txt");
		fileName.createNewFile();
		//System.out.println(fileName.exists());
		PrintWriter pw=new PrintWriter(fileName);
		pw.println("!setValue(PROPERTYFILE_HRIS_FIELDS, \"hris_fields.properties\")\r\n" + 
				"!setValue(_displayDOB, \"false\")\r\n" + 
				"!ifb(!eval(!getProperty(\"hris.displaydob.all\", \"hris_fields.properties\"), STR_EQ_IC, \"true\"))\r\n" + 
				"	!setValue(_displayDOB, \"true\")\r\n" + 
				"!elseifb(!eval(!getProperty(!concat(\"hris.displaydob.\", loginUser.currentCapacity), \"hris_fields.properties\"), STR_EQ_IC, \"true\"))\r\n" + 
				"	!setValue(_displayDOB, \"true\")\r\n" + 
				"!endifb\r\n" + 
				"!setValue(_isHrisModulesPurchased,\"false\")\r\n" + 
				"!ifb(!checkModulePurchased(\"HRIS\"))\r\n" + 
				"	!setValue(_isHrisModulesPurchased,\"true\")\r\n" + 
				"!endifb");
		pw.println("<table class=\"li_common_form2\" width=\"350px;\">\r\n" + 
				"		<tr>\r\n" + 
				"			<td>\r\n" + 
				"				!getLabel(\"l.timeline.startDate\") :&nbsp;\r\n" + 
				"				<input type=\"text\" class=\"cal\" name=\"date_startDate\" value=\"\" id=\"filterStartDate\">\r\n" + 
				"			</td>\r\n" + 
				"		</tr>\r\n" + 
				"		<tr>\r\n" + 
				"			<td>\r\n" + 
				"				!getLabel(\"l.timeline.endDate\") :&nbsp;&nbsp;\r\n" + 
				"				<input type=\"text\" class=\"cal\" name=\"date_endDate\" value=\"\" id=\"filterEndDate\">\r\n" + 
				"			</td>\r\n" + 
				"		</tr>\r\n" + 
				"		<tr>\r\n" + 
				"			<td align=\"right\">\r\n" + 
				"				<a href=\"#\" onclick=\"javascript:drawEmpCareerLeaveDashboardByCustomDate();return false;\" class=\"li_button\">\r\n" + 
				"					!getLabel(\"l.go\")\r\n" + 
				"				</a>\r\n" + 
				"			</td>\r\n" + 
				"		</tr>\r\n" + 
				"	</table>");
		pw.flush();
		pw.close();
		FileWriter fw=new FileWriter(fileName, true);
		PrintWriter pw2=new PrintWriter(fw);
		pw2.println("!elseifb(!eval(dwrMode, STR_EQ_IC, \"viewRiskIdentification\"))\r\n" + 
				"	<div class=\"row\">\r\n" + 
				"		<div class=\"col-md-12\">\r\n" + 
				"			<div id=\"viewRiskIdentificationHeaderDiv\" class=\"li_common_header_nonbg ll_bb open-box\">\r\n" + 
				"				<span>\r\n" + 
				"					!getLabel(\"l.atRisk\")\r\n" + 
				"				</span>\r\n" + 
				"			</div>\r\n" + 
				"		</div>\r\n" + 
				"		<div class=\"clear\"></div>\r\n" + 
				"		<div class=\"col-md-12 new_profilesection\">\r\n" + 
				"			!setValue(renderTableHeaderArray, !createArray(2))\r\n" + 
				"			!setValue(renderTableDataArray, !createArray(2))\r\n" + 
				"			!setValue(_curRow, 0)\r\n" + 
				"\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 0, \"l.capturedBy\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 1, \"\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 2, \"\")\r\n" + 
				"			!inc(_curRow)\r\n" + 
				"\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 0, \"l.notes.addedOn\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 1, \"\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 2, \"\")\r\n" + 
				"			!inc(_curRow)\r\n" + 
				"\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 0, \"l.action\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 1, \"menulist,mix\")\r\n" + 
				"			!setValue(renderTableHeaderArray, _curRow, 2, \"\")\r\n" + 
				"			!inc(_curRow)\r\n" + 
				"			!setValue(_wfId ,\"-1\")\r\n" + 
				"			!setValue(_wfQuery, !concat(\"SELECT WF_ID  FROM WORKFLOW WHERE WF_TYPE = 'AtRiskIdentification'  AND STATE ='signed-off' AND ARCHIEVED = 0 AND EMPID = \",contextEmployee.empId))\r\n" + 
				"			!setValue(_wfQueryDtl, !runQuery(_wfQuery))\r\n" + 
				"			!ifb(!eval(!notIsNull(_wfQueryDtl), AND, !eval(!length(_wfQueryDtl), >, 0)))\r\n" + 
				"				!setValue(_wfId, !getValue(_wfQueryDtl, 0,0))\r\n" + 
				"				!setValue(_query, !concat(\"SELECT CAPTUREDBY, ADDEDON  FROM  ATRISKIDENTIFICATION WHERE STATUS = 0 AND ATRISKIDENTIFICATIONID = \",_wfId ))\r\n" + 
				"				!setValue(_riskIdentificationDtl, !runQuery(_query))\r\n" + 
				"			!endifb\r\n" + 
				"\r\n" + 
				"			!ifb(!eval(!notIsNull(_riskIdentificationDtl), AND, !eval(!length(_riskIdentificationDtl),==,1)))\r\n" + 
				"				<input type=\"hidden\" id=\"dataRecordsCount\" value=\"!length(_riskIdentificationDtl)\">\r\n" + 
				"				<input type=\"hidden\" id=\"atRiskIdentificationId\" value=\"-1\">\r\n" + 
				"				<input type=\"hidden\" id=\"atRiskIdentificationId_empProfile\" value=\"-1\">\r\n" + 
				"				!iterator(_index, _riskIdentificationDtl)\r\n" + 
				"					!setValue(_capturedBy, !getValue(_riskIdentificationDtl, _index,0))\r\n" + 
				"					!setValue(_addedOn, !getValue(_riskIdentificationDtl, _index,1))\r\n" + 
				"\r\n" + 
				"					!setValue(_curCol, 0)\r\n" + 
				"					!setValue(_capturedByInfo, !getEmployee(_capturedBy))\r\n" + 
				"					!setValue(renderTableDataArray, _index, _curCol, _capturedByInfo.empName)\r\n" + 
				"					!inc(_curCol)\r\n" + 
				"\r\n" + 
				"					!setValue(renderTableDataArray, _index, _curCol, !eval(_addedOn,TO_STRING))\r\n" + 
				"					!inc(_curCol)\r\n" + 
				"\r\n" + 
				"					!setValue(_curUrls, !createArray(1))\r\n" + 
				"					!setValue(_curUrlIndex, 0)\r\n" + 
				"\r\n" + 
				"					!setValue(_curUrls, _curUrlIndex, \"function@l.edit@javascript:viewAtRiskIdentificationFormGlobalInvocation('!getValue(_wfId)');return false;\")\r\n" + 
				"					!inc(_curUrlIndex)\r\n" + 
				"					!setValue(_curUrls, _curUrlIndex, \"function@l.addNote@javascript:document.getElementById('atRiskIdentificationId').value=!getValue(_wfId);showCommonNotesGlobalInvocation('!getValue(contextEmployee.empId)','AtRiskIdentification');return false;\")\r\n" + 
				"					!inc(_curUrlIndex)\r\n" + 
				"					!setValue(_tempUrl, !concat(\"runet.do?activity=submit&cdmtHeaderBar=false&enc=\", !encrypt_decrypt(\"encrypt\", !concat(\"mod=atRiskIdentification&secondaryVar=extendedDwrMode^getAllCommontesForSelectedAtRisk,atRiskIdentificationId^\", !getValue(_wfId), \",currEmpId^\", contextEmployee.empId , \",displayLeftPanel^false\"))))\r\n" + 
				"					!setValue(_curUrls, _curUrlIndex, \"urlInNewWindow@l.view@!getValue(_tempUrl)\")\r\n" + 
				"\r\n" + 
				"					!setValue(renderTableDataArray, _index, _curCol, _curUrls)\r\n" + 
				"				!endIterator\r\n" + 
				"			!endifb\r\n" + 
				"			!processTextFileData(\"page/common/renderTable.htm\", \"showHeader=true,displayTableBorder=false,showRowSeparator=true,_showFirstColumnItemAsUrl=no\")\r\n" + 
				"		</div>\r\n" + 
				"	</div>\r\n" + 
				"	<div class=\"clear\"></div>");
		pw2.flush();
		pw2.close();
		BufferedReader br=new BufferedReader(new FileReader(fileName));
		String read=br.readLine();
		while(read!=null) {
			System.out.println(read);
			read=br.readLine();
		}
	}

	
}

