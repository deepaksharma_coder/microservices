package com.Microservice.demo.ServiceImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Microservice.demo.Dao.EmployeeProjectDao;
import com.Microservice.demo.DaoImpl.EmployeeDaoImpl;
import com.Microservice.demo.Dto.EmployeeDto;
import com.Microservice.demo.Model.EmployeeEntity;
import com.Microservice.demo.Model.ProjectEntity;
import com.Microservice.demo.Service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeDaoImpl employeeDaoImpl;
	
	@Autowired
	private EmployeeProjectDao employeeProject;
	
	@Autowired
	private EmployeeProjectDao empProject;
	
	@Value("${myimagelocation.dev.location}")
	private String location;
	
	@Override
	public Map<String, Object> saveEmp(Map<String, Object> map) {
		
		//ProjectEntity project=new ObjectMapper().convertValue(map.get("projectDetails"), ProjectEntity.class);
		//map.remove("projectDetails");
		EmployeeEntity employee=new ObjectMapper().convertValue(map, EmployeeEntity.class);
		// TODO Auto-generated method stub
		employeeDaoImpl.save(employee);
		return employeeDaoImpl.getEmpById(employee.getId());
	}

	@Override
	public List<Map<String, Object>> getAllEmpRecords() {
		// TODO Auto-generated method stub
		List<Map<String,Object>> persistUser=employeeDaoImpl.getAllEmpRecords();
		//SimpleDateFormat sm=new SimpleDateFormat("dd/MM/yyyy");
		//Date dt=new Date();
		//String currentDate=sm.format(dt);
		//LocalDate current=LocalDate.parse(currentDate);
		DateTimeFormatter formatter = null;
		Period period =null;
		int age=0;
		int years=0;
		int months=0;
		int days=0;
		for(Map<String,Object> ob:persistUser){
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			period=Period.between(LocalDate.parse(ob.get("dob").toString(),formatter), LocalDate.now());
			years=period.getYears();
			months=period.getMonths();
			days=period.getDays();
			//age=years+months/12+days/30;
			/*System.out.println(Instant.now().atZone(ZoneId.of("America/New_York")));
			System.out.println(Instant.now().atZone(ZoneId.of("Asia/Calcutta")));
			System.out.println(Instant.now().atZone(ZoneId.of("Asia/Tokyo")));*/
			//age=period.getYears()+period.getMonths()+period.getDays();
			ob.put("dob", period.getYears()+" Years "+period.getMonths()+" months "+period.getDays()+" days ");
			System.out.println(ob.get("picOriginalName").toString());
			String[] fileName=ob.get("picOriginalName").toString().split("\\.");
			//String extension=ob.get("picOriginalName").toString().split("\\.")[1];
			//System.out.println(fileName+""+extension);
			ob.put("picOriginalName", fileName[0]+"_"+ob.get("id").toString()+"."+fileName[1]);
			
		}
		return persistUser;
	}

	@Override
	public void deleteEmpById(long id) {
		// TODO Auto-generated method stub
		employeeDaoImpl.deleteEmpById(id);
	}

	@Override
	public void updateEmpById(long id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		employeeDaoImpl.updateEmpById(id, map);
	}

	@Override
	public Map<String, Object> getEmpByStateorUser(String state, String user) {
		// TODO Auto-generated method stub
		return employeeDaoImpl.getEmpByStateoruser(state, user);
	}

	@Override
	public Map<String, Object> getEmpRecordById(long id) {
		// TODO Auto-generated method stub
		return employeeDaoImpl.getEmpById(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveEmp(EmployeeDto employeeDto) {
		// TODO Auto-generated method stub
		Map<String,Object> employee=new HashMap<String,Object>();
		Map<String,Object> project=new HashMap<String,Object>();
		employee=new ObjectMapper().convertValue(employeeDto, Map.class);
		employee.remove("projectDetails");
		EmployeeEntity emp=new ObjectMapper().convertValue(employee, EmployeeEntity.class);
		employeeDaoImpl.save(emp);
		if(emp.getId()!=null) {
			//project.put("employee_id", emp.getId());
			for(int i=0;i<employeeDto.getProjectDetails().length;i++) {
				project.put("projectName", employeeDto.getProjectDetails()[i].getProjectName());
				project.put("projectType", employeeDto.getProjectDetails()[i].getProjectType());
				project.put("duration", employeeDto.getProjectDetails()[i].getDuration());
				project.put("employee", emp);
				empProject.saveEmpProject(new ObjectMapper().convertValue(project, ProjectEntity.class));
			}
		}
		
		return new ObjectMapper().convertValue(employeeDto, Map.class);
	}

	@Override
	public Map<String, Object> getEmpProjectByEmpId(Long id) {
		// TODO Auto-generated method stub
		Map<String,Object> employeeData=new LinkedHashMap<>();
		employeeData=employeeDaoImpl.getEmpById(id);
		employeeData.put("ProjectDetails", employeeProject.getEmpProjectByEmpId(id));
		try {
			Path path=Paths.get("F:/MyFiles/demo.txt");
			if(!Files.exists(path)) {
				Files.createDirectory(path);
				Files.createFile(path);
			}
			String str="EmployeeId,UserName,Password,State,Country,Dob,ZipCode,ProfileName\n";
			str+=employeeData.get("id").toString()
					+","+employeeData.get("user").toString()
					+","+employeeData.get("password").toString()
					+","+employeeData.get("state").toString()
					+","+employeeData.get("country").toString()
					+","+employeeData.get("dob").toString()
					+","+employeeData.get("zipcode").toString()
					+","+employeeData.get("picOriginalName").toString();
			Files.write(path, str.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(path);
		return employeeData;
	}

}
