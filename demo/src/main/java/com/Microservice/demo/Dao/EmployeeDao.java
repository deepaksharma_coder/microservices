package com.Microservice.demo.Dao;

import java.util.List;
import java.util.Map;

import com.Microservice.demo.Model.EmployeeEntity;

public interface EmployeeDao {
	public void saveEmployee(EmployeeEntity employee);
	public Map<String,Object> getEmpById(long id);
	public List<Map<String,Object>> getAllEmpRecords();
	public Map<String,Object> getEmpByStateoruser(String state,String user);
	public void deleteEmpById(long id);
	public void updateEmpById(long id,Map<String,Object> map);

}
