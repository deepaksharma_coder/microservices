package com.Microservice.demo.Dao;

import java.util.List;
import java.util.Map;

import com.Microservice.demo.Model.ProjectEntity;

public interface EmployeeProjectDao {
	public void saveEmpProject(ProjectEntity project);
	public List<Map<String,Object>> getEmpProjectById(Long id);
	public List<Map<String,Object>> getEmpProjectByEmpId(Long id);

}
