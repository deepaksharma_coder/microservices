package com.Microservice.demo.Model;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="employee",uniqueConstraints={@UniqueConstraint(columnNames={"user"})})
public class EmployeeEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="user",unique=true,nullable=false)
	private String user;
	
	@Column(name="password",nullable=false,length=6)
	private String password;
	
	@Column(name="state",nullable=false)
	private String state;
	
	@Column(name="country",nullable=false)
	private String country;
	
	@Column(name="dob",nullable=false)
	private String dob;
	
	@Column(name="zipcode",nullable=false)
	private String zipcode;
	
	@Column(name = "pic_original_name")
	private String picOriginalName;
	
	@OneToMany(mappedBy="employee",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JsonManagedReference(value="employee-project")
	private Set<ProjectEntity> project;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPicOriginalName() {
		return picOriginalName;
	}

	public void setPicOriginalName(String picOriginalName) {
		this.picOriginalName = picOriginalName;
	}

	public Set<ProjectEntity> getProject() {
		return project;
	}

	public void setProject(Set<ProjectEntity> project) {
		this.project = project;
	}
	

}
