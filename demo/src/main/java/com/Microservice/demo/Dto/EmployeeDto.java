package com.Microservice.demo.Dto;

import com.Microservice.demo.Model.ProjectEntity;

public class EmployeeDto {
	private long id;
	private String user;
	private String password;
	private String state;
	private String country;
	private String dob;
	private String zipcode;
	private String picOriginalName;
	private ProjectEntity[] projectDetails;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getPicOriginalName() {
		return picOriginalName;
	}
	public void setPicOriginalName(String picOriginalName) {
		this.picOriginalName = picOriginalName;
	}
	public ProjectEntity[] getProjectDetails() {
		return projectDetails;
	}
	public void setProjectDetails(ProjectEntity[] projectDetails) {
		this.projectDetails = projectDetails;
	}
	
	
	
	
	
	
	

}
