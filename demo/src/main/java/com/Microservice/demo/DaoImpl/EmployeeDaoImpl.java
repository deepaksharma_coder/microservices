package com.Microservice.demo.DaoImpl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.Microservice.demo.Dao.BaseDao;
import com.Microservice.demo.Dao.EmployeeDao;
import com.Microservice.demo.Model.EmployeeEntity;

@Repository("employeeDaoImpl")
public class EmployeeDaoImpl extends BaseDao<Long,EmployeeEntity> implements EmployeeDao{

	private ProjectionList getEmpProjection(){
		return Projections.projectionList()
				.add(Projections.property("user"),"user")
				.add(Projections.property("password"),"password")
				.add(Projections.property("dob"),"dob")
				.add(Projections.property("state"),"state")
				.add(Projections.property("country"),"country")
				.add(Projections.property("zipcode"),"zipcode")
				.add(Projections.property("id"),"id")
				.add(Projections.property("picOriginalName"),"picOriginalName");
				//.add(Projections.property("project.projectName"),"projectName")
				//.add(Projections.property("project.type"),"projectType")
				//.add(Projections.property("project.duration"),"projectDuration");
		
	}
	@Override
	public void saveEmployee(EmployeeEntity employee) {
		// TODO Auto-generated method stub
		save(employee);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getEmpById(long id) {
		// TODO Auto-generated method stub
		return (Map<String,Object>) createCriteria()
				.add(Restrictions.eq("id", id))
				.setProjection(getEmpProjection())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
	}

	@Override
	public void deleteEmpById(long id) {
		// TODO Auto-generated method stub
		String hql="DELETE FROM EmployeeEntity where id=:id";
		createQuery(hql).setParameter("id", id).executeUpdate();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void updateEmpById(long id,Map<String,Object> map) {
		// TODO Auto-generated method stub
		//Date dt=new Date();
		String hql="update EmployeeEntity set user=:user,password=:password,dob=:dob,"
				+ "state=:state,country=:country,zipcode=:zipcode where id=:id";
		createQuery(hql)
		.setParameter("user", map.get("user").toString())
		.setParameter("password",  map.get("password").toString())
		.setParameter("dob",  map.get("dob").toString())
		.setParameter("state",  map.get("state").toString())
		.setParameter("country",  map.get("country").toString())
		.setParameter("zipcode",  map.get("zipcode").toString())
		.setParameter("id",  id).executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getAllEmpRecords() {
		// TODO Auto-generated method stub
		return (List<Map<String,Object>>)createCriteria()
				.setProjection(getEmpProjection())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getEmpByStateoruser(String state, String user) {
		// TODO Auto-generated method stub
		Criteria cr3=createCriteria();
		Criterion cr=Restrictions.conjunction(Restrictions.like("state", state+"%",MatchMode.START),Restrictions.like("state", state+"%",MatchMode.START));
		Criterion cr1=Restrictions.disjunction(Restrictions.like("user", user+"%",MatchMode.START),Restrictions.like("user", user+"%",MatchMode.START));
		return (Map<String,Object>) cr3.add(Restrictions.or(cr,cr1)).setProjection(getEmpProjection()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).uniqueResult();
		
		
	}

}
