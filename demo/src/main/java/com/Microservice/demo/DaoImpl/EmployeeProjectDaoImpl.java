package com.Microservice.demo.DaoImpl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.Microservice.demo.Dao.BaseDao;
import com.Microservice.demo.Dao.EmployeeProjectDao;
import com.Microservice.demo.Model.ProjectEntity;

@Repository("employeeProjectDaoImpl")
public class EmployeeProjectDaoImpl extends BaseDao<Long,ProjectEntity> implements EmployeeProjectDao {
	
	@SuppressWarnings("unused")
	private Criteria customCriteria() {
		return createCriteria().createAlias("employee", "employee", JoinType.INNER_JOIN);
	}
	
	private ProjectionList getEmpProjectProjection(){
		return Projections.projectionList()
				.add(Projections.property("projectName"),"projectName")
				.add(Projections.property("projectType"),"projectType")
				.add(Projections.property("duration"),"duration")
				.add(Projections.property("employee.user"),"user")
				.add(Projections.property("employee.password"),"password")
				.add(Projections.property("employee.state"),"state")
				.add(Projections.property("employee.country"),"country")
				.add(Projections.property("employee.zipcode"),"zipcode")
				.add(Projections.property("employee.id"),"empId")
				.add(Projections.property("employee.picOriginalName"),"picOriginalName");
		
	}

	@Override
	public void saveEmpProject(ProjectEntity project) {
		// TODO Auto-generated method stub
		save(project);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getEmpProjectById(Long id) {
		// TODO Auto-generated method stub
		return (List<Map<String,Object>>) customCriteria()
				.add(Restrictions.eq("employee.id", id))
				.setProjection(getEmpProjectProjection())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getEmpProjectByEmpId(Long id) {
		// TODO Auto-generated method stub
		return (List<Map<String,Object>>) customCriteria()
				.add(Restrictions.eq("employee.id", id))
				.setProjection(Projections.projectionList()
						.add(Projections.property("projectName"),"projectName")
						.add(Projections.property("projectType"),"projectType")
						.add(Projections.property("duration"),"duration"))
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
				
	}
	
	

}
