package com.Microservice.demo.Service;

import java.util.List;
import java.util.Map;

import com.Microservice.demo.Dto.EmployeeDto;

public interface EmployeeService {
	public Map<String,Object> saveEmp(Map<String,Object> map);
	public Map<String,Object> getEmpRecordById(long id);
	public List<Map<String,Object>> getAllEmpRecords();
	public Map<String,Object> getEmpByStateorUser(String state,String user);
	public void deleteEmpById(long id);
	public void updateEmpById(long id,Map<String,Object> map);
	public Map<String, Object> saveEmp(EmployeeDto employeeDto);
	public Map<String,Object> getEmpProjectByEmpId(Long id);

}
