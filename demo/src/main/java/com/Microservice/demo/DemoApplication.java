package com.Microservice.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.Microservice.demo.Controller.MyNewModule;
import com.Microservice.demo.Dto.EmployeeDto;
import com.Microservice.demo.ServiceImpl.EmailNotiFicationImpl;

@SpringBootApplication
public class DemoApplication {
	static ApplicationContext applicationContext;

	public static void main(String[] args) {
		applicationContext=SpringApplication.run(DemoApplication.class, args);
	}

	@Bean(name="myNewModuleBean")
	public MyNewModule getModule(){
		return new MyNewModule();
	}
	
	@Bean(name="emailNotification")
	//@ConditionalOnProperty(prefix="notification",name="service",havingValue="email")
	//@ConditionalOnExpression(value="${notification.service.email} and ${notification.service.sms}")
	//@ConditionalOnBean(MyNewModule.class)
	@ConditionalOnJava(value=JavaVersion.EIGHT)
	public EmailNotiFicationImpl emailNotification(){
		return new EmailNotiFicationImpl();
	}
	
	/*@Bean(name="smsNotification")
	@ConditionalOnProperty(prefix="notification",name="service",havingValue="sms")
	public SmsNotificationImpl smsNotification(){
		return new SmsNotificationImpl();
	}*/
	
	@Bean
	public EmployeeDto employeeDto(){
		return new EmployeeDto();
	}
	
	
	@Bean
	public CommandLineRunner commnadLineRunner(ApplicationContext ctc){
		for(String beans:ctc.getBeanDefinitionNames()){
			System.out.println("This is my beans.."+beans);
			
		}
		return null;
	}
	
	

}
