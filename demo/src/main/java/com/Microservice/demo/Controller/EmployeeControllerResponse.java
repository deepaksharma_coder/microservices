package com.Microservice.demo.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.Microservice.demo.Service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class EmployeeControllerResponse {

	@Autowired
	private EmployeeService employeeService;
	
	
	@PostMapping("/delete")
	public ResponseEntity<String> deleteData(@RequestBody Map<String,Object> map) throws JsonProcessingException {
		employeeService.deleteEmpById(Long.parseLong(map.get("id").toString()));
		return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Record deleted sucessfully"),HttpStatus.OK);
		
	}
	@RequestMapping(value="/saveEmployeeAjax",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> saveEmpData(@RequestBody Map<String,Object> map) throws JsonProcessingException{
		//String msg="";
		try {
			map.put("picOriginalName", "test.txt");
			Map<String,Object> persisitEmployeeAjax=employeeService.saveEmp(map);
			persisitEmployeeAjax.put("msg", "Records Saved Successfully...");
			//msg="Records Saved Successfully...";
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(persisitEmployeeAjax),HttpStatus.OK);
		}catch(DataIntegrityViolationException c){
			return new ResponseEntity<String>(
					new ObjectMapper().writeValueAsString(
							new Response("Conflict","Duplicate Entry","Duplicate Entry","Conflict"))
					,HttpStatus.CONFLICT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Data not saved..Something went wrong....Please contact support"),HttpStatus.OK);
		}
		
		
	}
	
	/*@GetMapping("/empRecords")
	public ResponseEntity<String> getUserDetails() throws JsonProcessingException{
		List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
		return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(saveedData), HttpStatus.OK);
	}*/
}
