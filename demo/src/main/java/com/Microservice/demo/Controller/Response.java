package com.Microservice.demo.Controller;

import java.util.UUID;

public class Response {
	private long timestamp = UUID.randomUUID().getMostSignificantBits();
	private String error;
	private String message;
	private String developerMessage;
	private String status;
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeveloperMessage() {
		return developerMessage;
	}
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Response(String error, String message, String developerMessage, String status) {
		//this.timestamp = timestamp;
		this.error = error;
		this.message = message;
		this.developerMessage = developerMessage;
		this.status = status;
	}
	
	
	

}
