package com.Microservice.demo.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.Microservice.demo.Dto.EmployeeDto;
import com.Microservice.demo.Service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//@RestController
@Controller
public class EmployeeController {
	
	//@Value("${myimagelocation.dev.location}")
	//private String location;
	
	@Autowired
	private MyLocationProperties custom;
	
	@Autowired
	private EmployeeService employeeService;
	
	
	@Autowired
	private EmployeeDto employeeDto;
	
	Path currentPath=Paths.get(".");
	Path absolutePath=currentPath.toAbsolutePath();
	
	@PostMapping("/saveEmpData")
	public ResponseEntity<String> saveEmpData(@RequestBody EmployeeDto employeeDto) throws JsonProcessingException{
		try {
			Map<String,Object> persisitEmployee=employeeService.saveEmp(employeeDto);
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(persisitEmployee), HttpStatus.OK);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Internal server error"), HttpStatus.OK);
		}
		
		
	}
	@RequestMapping(value="/saveEmployee",method=RequestMethod.POST)
	public ModelAndView saveEmployeeRecord(@ModelAttribute("employeeDto") EmployeeDto employeeDto,@RequestParam("uploadfile") MultipartFile file,ModelAndView model){
		try {
			Map<String,Object> map=new HashMap<>();
			if(employeeDto.getId()!=0){
				map.put("user", employeeDto.getUser());
				map.put("password", employeeDto.getPassword());
				map.put("zipcode", employeeDto.getZipcode());
				map.put("state", employeeDto.getState());
				map.put("country", employeeDto.getCountry());
				map.put("dob", employeeDto.getDob());
				map.put("id", employeeDto.getId());
				employeeService.updateEmpById(employeeDto.getId(), map);
				/*if(file!=null){
					//System.out.println(file.get);
					String[] files=employeeDto.getPicOriginalName().split("\\.");
					Path path=Paths.get(absolutePath + custom.getLocation() +files[0]+"_"+employeeDto.getId()+"."+files[1]);
					System.out.println(path+"..."+custom.getLocation());
					Files.write(path, file.getBytes());
				}*/
				List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
				model.addObject("listEmployees", saveedData);
				model.setViewName("redirect:/homePage");
				//return model;
			}
			else{
				map.put("user", employeeDto.getUser());
				map.put("password", employeeDto.getPassword());
				map.put("state", employeeDto.getState());
				map.put("country", employeeDto.getCountry());
				map.put("dob", employeeDto.getDob());
				map.put("zipcode", employeeDto.getZipcode());
				map.put("picOriginalName", file.getOriginalFilename());
				map.put("project", employeeDto.getProjectDetails());
				Map<String,Object> persisitEmployee=employeeService.saveEmp(map);
				if(file!=null){
					//System.out.println(file.get);
					String[] files=persisitEmployee.get("picOriginalName").toString().split("\\.");
					Path path=Paths.get(absolutePath + custom.getLocation() +files[0]+"_"+persisitEmployee.get("id").toString()+"."+files[1]);
					System.out.println(path+"..."+custom.getLocation());
					Files.write(path, file.getBytes());
				}
				List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
				model.addObject("listEmployees", saveedData);
				model.setViewName("redirect:/homePage");
				//return model;
			 }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return model;
		
	}
	
	@RequestMapping(value="/updateRecords/{id}",consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateRecords(@PathVariable("id") long id,@RequestBody Map<String,Object> map) throws JsonProcessingException{
		try{
			employeeService.updateEmpById(id, map);
			List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(saveedData), HttpStatus.OK);
		}catch(Exception e2){
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Internal server error.."+e2.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@GetMapping("/deleteRecords/{id}")
	public String deleteRecords(@PathVariable("id") long id) throws JsonProcessingException{
		employeeService.deleteEmpById(id);
		//List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
		//modelView.setViewName("homePage");
		return "redirect:/homePage";
		
	}
	
	/*@PostMapping("/delete")
	@ResponseBody
	public Model deleteData(@RequestBody Map<String,Object> map,Model model) {
		employeeService.deleteEmpById(Long.parseLong(map.get("id").toString()));
		model.addAttribute("msg", "Record deleted sucessfully");
		return model;
		
	}*/
	
	@GetMapping("/empRecords")
	public ResponseEntity<String> getUserDetails() throws JsonProcessingException{
		List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
		return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(saveedData), HttpStatus.OK);
	}
	
	@GetMapping("/getEmpProjectDetails/{empId}")
	public ResponseEntity<String> getEmpProjects(@PathVariable("empId") Long empId) throws JsonProcessingException{
		try {
			Map<String,Object> persistData=employeeService.getEmpProjectByEmpId(empId);
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(persistData), HttpStatus.OK);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Internal Server error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@GetMapping("/getImageDownload/{name}")
	public ResponseEntity<Resource> getImageDownload(@PathVariable("name") String name,HttpServletRequest request) throws IOException{
		Path currentPath=Paths.get(".");
		Path path1=currentPath.toAbsolutePath();
	    String fileBasePath =path1+custom.getLocation()+name;
	    File file = new File(fileBasePath);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=img.jpg");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
		
	}
	@GetMapping("/downloads")
	public ResponseEntity<Resource> downloads() throws IOException{
		Path path=Paths.get("F:/MyFiles/demo.txt");
		byte[] readContent=Files.readAllBytes(path);
		HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=demo.csv");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
		return ResponseEntity.ok()
				.headers(header)
                .contentLength(readContent.length)
                .contentType(MediaType.TEXT_PLAIN)
                .body(new ByteArrayResource(Files.readAllBytes(path))); 
		
	}
	@GetMapping("/getDetails/{state}/{name}")
	public ResponseEntity<String> getDetails(@PathVariable("state") String state,@PathVariable("name") String name) throws JsonProcessingException{
		try {
			Map<String,Object> persisitEmp=employeeService.getEmpByStateorUser(state, name);
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(persisitEmp), HttpStatus.OK);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("Internal server error.."+e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/homePage")
	public ModelAndView getEmployeesDetails(ModelAndView modelView){
		modelView.setViewName("homePage");
		List<Map<String,Object>> saveedData=employeeService.getAllEmpRecords();
		modelView.addObject("listEmployees", saveedData);
		modelView.setViewName("homePage");
		return modelView;	
	}
	
	@GetMapping("/showNewEmployeeForm")
	public ModelAndView showNewEmployeeForm(ModelAndView modelView){
		modelView.setViewName("addNewEmployee");
		modelView.addObject("employee", employeeDto);
		return modelView;
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView updateRedirect(@PathVariable("id") long id,ModelAndView modelView){
		Map<String,Object> persisitEmp=employeeService.getEmpRecordById(id);
		EmployeeDto employee=new ObjectMapper().convertValue(persisitEmp, EmployeeDto.class);
		modelView.setViewName("editPage");
		modelView.addObject("employee", employee);
		return modelView;
	}
	@PostMapping("/update/updateData")
	public ResponseEntity<String> editPage(@RequestBody Map<String,Object> map) throws JsonProcessingException{
		long id=Long.valueOf(map.get("id").toString());
		employeeService.updateEmpById(id, map);
		return new ResponseEntity<String>(new ObjectMapper().
				writeValueAsString(new Response("successFully saved..", "successFully saved..", "successFully saved..", "200")), HttpStatus.OK);
	}
	
	
	

}
