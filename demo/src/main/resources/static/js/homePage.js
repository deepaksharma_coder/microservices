function myFunction(event){
	event.preventDefault();
	var array=new Array('user','password','state','country','date','zipcode','projectName','projectType','duration');
	var obj=new Object();
	for(var i=0;i<array.length;i++){
		var cur=document.getElementById(array[i])
		if(cur!=null && cur.value.trim().length==0){
			alert("Field Mandatory.."+array[i]);
			return false;
		}
		
	}
	obj.user=document.getElementById('user').value;
	obj.password=document.getElementById('password').value;
	obj.state=document.getElementById('state').value;
	obj.country=document.getElementById('country').value;
	obj.dob=document.getElementById('date').value;
	obj.zipcode=document.getElementById('zipcode').value;
	obj.projectName=document.getElementById('projectName').value;
	obj.projectType=document.getElementById('projectType').value;
	obj.duration=document.getElementById('duration').value;
	var json = JSON.stringify(obj);
	$.ajax({
	      type: 'POST',
	      url: "saveEmployeeAjax",
	      data: json,
	      dataType: 'json',
	      processData: false,
	      contentType: 'application/json',
	      success: function(resultData) {
	    	 console.log(resultData);
	    	  var result=JSON.stringify(resultData);
	    	  var obj2=JSON.parse(result);
	    	  $('#error').html(obj2.msg);
	    	  document.getElementById("error").style.color = "green";
	    	  window.location = "http://localhost:8080/homePage";
	       },
	      error:function(resultData){
	    	  var messages=resultData.responseJSON.message+resultData.responseJSON.developerMessage+resultData.responseJSON.status+"..status.."+resultData.status;
	    	  $('#error').html(messages);
	    	  document.getElementById("error").style.color = "red";
	      }
	});
}
function deleteData(data){
	var object=new Object();
	object.id=data;
	$.ajax({
	      type: 'POST',
	      url: "delete",
	      dataType: "json",
	      data: JSON.stringify(object),
	      processData: false,
	      contentType: 'application/json',
	      success: function(resultData) {
	    	  location.reload(); 
	       },
	      error:function(resultData){
	    	  console.log(resultData);
	      }
	});
}
function viewHomePage(){
	$.ajax({
	      type: 'GET',
	      url: "homePage",
	      //dataType: "json",
	      //data: JSON.stringify(object),
	      //processData: false,
	      //contentType: 'application/json',
	      success: function(resultData) {
	    	 console.log("SUCCESS");
	       },
	      error:function(resultData){
	    	  console.log("FAILURE");
	      }
	});
}
function updateData(event){
	event.preventDefault();
	var array=new Array('user','password','state','country','date','zipcode');
	var obj=new Object();
	for(var i=0;i<array.length;i++){
		var cur=document.getElementById(array[i])
		if(cur!=null && cur.value.trim().length==0){
			alert("Field Mandatory.."+array[i]);
			return false;
		}
		
	}
	var employeeId=document.getElementById("empId").value;
	var obj=new Object();
	obj.id=employeeId;
	obj.user=document.getElementById('user').value;
	obj.password=document.getElementById('password').value;
	obj.state=document.getElementById('state').value;
	obj.country=document.getElementById('country').value;
	obj.dob=document.getElementById('date').value;
	obj.zipcode=document.getElementById('zipcode').value;
	var json = JSON.stringify(obj);
	$.ajax({
	      type: 'POST',
	      url: "updateData",
	      dataType: "json",
	      data: JSON.stringify(obj),
	      processData: false,
	      contentType: 'application/json',
	      success: function(resultData) {
		     console.log(resultData);
	    	 // var messages=resultData.responseJSON.message+resultData.responseJSON.developerMessage+resultData.responseJSON.status+"..status.."+resultData.status;
	    	  $('#succ').html(resultData.message);
	    	  document.getElementById("succ").style.color = "red"; 
              window.location = "http://localhost:8080/homePage";

	       },
	      error:function(resultData){
	    	  console.log(resultData);
	      }
	});
}
